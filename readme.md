# Assistance

Setiap orang pilih salah satu problem yang belum terjawab, kerjakan, dan kirim source code ke `alfi.maulana.f@gmail.com`.
Setelah semua tim mengirim jawaban, hubungi asisten untuk waktu asistensi.

## Problem 1

Penggal tiap suku kata pada kalimat.

**Input**
``` sh
aku punya anjing kecil
suka berlarilari
```

**Output**
``` sh
a-ku pu-nya an-jing ke-cil
su-ka ber-la-ri-la-ri
```

## Problem 2

Input dari 0 - 99.

**Input**
```sh
5
11
89
```

**Output**
``` sh
lima
sebelas
delapan puluh sembilan
```

## Problem 3

Untuk tiap kata:
- Huruf awal dan akhir kapital.
- i = 1, r = 2, e = 3, a = 4, s = 5, g = 6, j = 7, b = 8, o = 0.

**Input**
``` sh
namaku budi
aku suka membaca
```

**Output**
``` sh
N4m4kU 8ud1
4kU 5uk4 M3m84c4
```

## Problem 4

Input `nol`, `satu`, ... , `sembilan puluh sembilan`.
Untuk kata salah output `-1`.

**Input**
```sh
lima puluh
sengbilan
```

**Output**
``` sh
50
-1
```

## Problem 5

Untuk tiap kata:
- output lowercase.
- 1 = i, 2 = r, 3 = e, 4 = a, 5 = s, 6 = g, 7 = j, 8 = b, 0 = o.


**Input**
``` sh
D1m4s an4K y4Ng b4Ik
```

**Output** (lowercase, untuk angka lihat aturan problem 3)
```sh
dimas anak yang baik
```